package com.tw.booking.controller;

import com.tw.booking.models.FlightDTO;
import com.tw.booking.models.FlightSearchCriteria;
import com.tw.booking.service.FlightService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@RestController
@RequestMapping("/api/flights")
public class FlightController {

    private FlightService flightService;

    @Autowired
    public FlightController(FlightService flightService) {
        this.flightService = flightService;
    }

    @GetMapping("/search")
    public Collection<FlightDTO>  searchFlights(FlightSearchCriteria flightSearchCriteria)
    {
        return flightService.searchFlights(flightSearchCriteria);
    }
}
