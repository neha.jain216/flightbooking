package com.tw.booking.entity;

import com.tw.booking.models.TravelType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Map;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class Flight {
    private int id;
    private String name;
    private String carrier;

    private Map<TravelType,TravelClass> travelClasses;

}
