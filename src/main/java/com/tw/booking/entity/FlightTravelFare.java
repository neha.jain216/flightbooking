package com.tw.booking.entity;

import com.tw.booking.models.TravelType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class FlightTravelFare {
    int id;
    int scheduleId;
    TravelType travelType;
    int availableSeats;
    double baseFare;
}
