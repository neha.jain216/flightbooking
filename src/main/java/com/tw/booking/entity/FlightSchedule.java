package com.tw.booking.entity;
import com.tw.booking.models.TravelType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.util.Map;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class FlightSchedule {
    private int id;
    private int flightId;
    private String source;
    private String destination;
    private LocalDate departureDateTime;

    private Map<TravelType,FlightTravelFare> flightTravelFares;
}
