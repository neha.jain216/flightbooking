package com.tw.booking.entity;

import com.tw.booking.models.TravelType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class TravelClass {
    private int id;
    private int flightId;
    private TravelType travelType;
    private int totalSeats;
}
