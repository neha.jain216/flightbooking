package com.tw.booking.service;

import com.tw.booking.entity.Flight;
import com.tw.booking.entity.FlightSchedule;
import com.tw.booking.models.FlightSearchCriteria;
import com.tw.booking.models.TravelType;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

import static com.tw.booking.models.TravelType.*;

@Component
class FareCalculator {
    private Map<TravelType,ICalculateFare> calculate;

    public FareCalculator() {
        calculate = new HashMap<>();
        calculate.put(FIRST_CLASS,new FirstClass());
        calculate.put(BUSINESS_CLASS, new BusinessClass());
        calculate.put(ECONOMY_CLASS,new EconomyClass());
    }

    double calculateTotalFare(Flight flight, FlightSchedule schedule, FlightSearchCriteria criteria) {

        ICalculateFare iCalculateFare = calculate.get(criteria.getTravelTypeValue());
        return iCalculateFare.calculateTotalFare(flight, schedule, criteria);
    }

}
