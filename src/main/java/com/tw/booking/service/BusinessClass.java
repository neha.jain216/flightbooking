package com.tw.booking.service;

import com.tw.booking.entity.Flight;
import com.tw.booking.entity.FlightSchedule;
import com.tw.booking.entity.FlightTravelFare;
import com.tw.booking.models.FlightSearchCriteria;

import java.time.DayOfWeek;
import java.time.LocalDate;

public class BusinessClass implements ICalculateFare {
    @Override
    public double calculateTotalFare(Flight flight, FlightSchedule schedule, FlightSearchCriteria criteria) {
        FlightTravelFare travelFare = schedule.getFlightTravelFares().get(criteria.getTravelTypeValue());

        double baseFare = travelFare.getBaseFare();
        double extra = 0;
        LocalDate departureDate = criteria.getDepartureDateValue();
        DayOfWeek dayOfWeek = departureDate.getDayOfWeek();
        if(dayOfWeek == DayOfWeek.MONDAY || dayOfWeek==DayOfWeek.FRIDAY || dayOfWeek==DayOfWeek.SUNDAY){
            extra = 0.4 * baseFare;
        }
        return ((baseFare+extra)*criteria.getPassengerCountValue());
    }
}
