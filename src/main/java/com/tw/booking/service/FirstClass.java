package com.tw.booking.service;

import com.tw.booking.entity.Flight;
import com.tw.booking.entity.FlightSchedule;
import com.tw.booking.entity.FlightTravelFare;
import com.tw.booking.models.FlightSearchCriteria;

import java.time.LocalDate;
import java.time.Period;

public class FirstClass implements ICalculateFare {
    @Override
    public double calculateTotalFare(Flight flight, FlightSchedule schedule, FlightSearchCriteria criteria) {
        FlightTravelFare travelFare = schedule.getFlightTravelFares().get(criteria.getTravelTypeValue());

        double baseFare = travelFare.getBaseFare();

        //Everyday 10% extra of base price
        int noOfDays = Period.between(LocalDate.now(), criteria.getDepartureDateValue()).getDays();
        double newBasePrice = baseFare + ((10-noOfDays) * 0.1 * baseFare);
        return newBasePrice * criteria.getPassengerCountValue();
    }
}
