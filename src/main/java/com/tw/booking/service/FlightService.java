package com.tw.booking.service;

import com.tw.booking.entity.Flight;
import com.tw.booking.entity.FlightSchedule;
import com.tw.booking.entity.FlightTravelFare;
import com.tw.booking.models.FlightDTO;
import com.tw.booking.models.FlightSearchCriteria;
import com.tw.booking.models.TravelType;
import com.tw.booking.repository.FlightRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class FlightService {

    private final FlightRepository flightRepository;
    private final FareCalculator fareCalculator;

    @Autowired
    public FlightService(FlightRepository flightRepository, FareCalculator fareCalculator) {
        this.flightRepository = flightRepository;
        this.fareCalculator = fareCalculator;
    }

    public Collection<FlightDTO> searchFlights(FlightSearchCriteria flightSearchCriteria) {
        List<FlightSchedule> allFlightSchedules = flightRepository.getAllFlightSchedules();
        List<FlightSchedule> flightSchedules = allFlightSchedules.stream()
                .filter(schedule -> schedule.getSource().equals(flightSearchCriteria.getSource()) &&
                        schedule.getDestination().equals(flightSearchCriteria.getDestination()) &&
                        flightAvailableOnDate(schedule,flightSearchCriteria) &&
                        flightClassHasSeats(schedule,flightSearchCriteria))
                .collect(Collectors.toList());

        return flightSchedules.stream()
                .map(schedule -> convertToFlightDTO(schedule,flightSearchCriteria))
                .collect(Collectors.toList());
    }

    private FlightDTO convertToFlightDTO(FlightSchedule schedule,FlightSearchCriteria flightSearchCriteria) {
        Flight flight = flightRepository.getFlightById(schedule.getFlightId()).orElse(null);
        return FlightDTO.builder()
                .id(flight.getId())
                .name(flight.getName())
                .origin(flightSearchCriteria.getSource())
                .destination(flightSearchCriteria.getDestination())
                .departureDate(flightSearchCriteria.getDepartureDateValue().format(DateTimeFormatter.ofLocalizedDate(FormatStyle.FULL)))
                .travelType(flightSearchCriteria.getTravelTypeValue())
                .totalFare(fareCalculator.calculateTotalFare(flight, schedule,flightSearchCriteria))
                .build();
    }
    private boolean flightAvailableOnDate(FlightSchedule schedule, FlightSearchCriteria criteria)
    {
        if(schedule.getDepartureDateTime().isEqual(criteria.getDepartureDateValue()))
        {
            if(criteria.getTravelTypeValue() == TravelType.FIRST_CLASS)
            {
                return Period.between(LocalDate.now(), criteria.getDepartureDateValue()).getDays() < 10
                        && Period.between(LocalDate.now(), criteria.getDepartureDateValue()).getDays() >= 0;
            }
            else if(criteria.getTravelTypeValue() == TravelType.BUSINESS_CLASS)
            {
                return Period.between(LocalDate.now(), criteria.getDepartureDateValue()).getDays() <= 28
                        && Period.between(LocalDate.now(), criteria.getDepartureDateValue()).getDays() >= 0;
            }
            else {
                return true;
            }
        }
        return false;
    }

    private boolean flightClassHasSeats(FlightSchedule schedule, FlightSearchCriteria flightSearchCriteria) {
        FlightTravelFare flightTravelFare = schedule.getFlightTravelFares()
                .get(flightSearchCriteria.getTravelTypeValue());
        return flightTravelFare != null && flightTravelFare.getAvailableSeats() >=
                flightSearchCriteria.getPassengerCountValue();
    }



}
