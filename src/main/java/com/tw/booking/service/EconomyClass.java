package com.tw.booking.service;

import com.tw.booking.entity.Flight;
import com.tw.booking.entity.FlightSchedule;
import com.tw.booking.entity.FlightTravelFare;
import com.tw.booking.models.FlightSearchCriteria;

public class EconomyClass implements ICalculateFare {
    @Override
    public double calculateTotalFare(Flight flight, FlightSchedule schedule, FlightSearchCriteria criteria) {
        FlightTravelFare travelFare = schedule.getFlightTravelFares().get(criteria.getTravelTypeValue());
        int totalSeats = flight.getTravelClasses().get(criteria.getTravelTypeValue()).getTotalSeats();
        int availableSeats = travelFare.getAvailableSeats();
        double baseFare = travelFare.getBaseFare();
        double extra=0;

        int first40 = (int)(totalSeats-(totalSeats*0.4));
        int next50 = (int)((totalSeats) -(totalSeats*0.5));

        if(availableSeats>=first40 && availableSeats<=totalSeats)
        {
            extra = 0;
        }
        if(availableSeats<(first40) && availableSeats>=next50 )
        {
            extra = baseFare*0.3;
        }
        if(availableSeats<(next50) && availableSeats>=1)
        {
            extra = baseFare*0.6;
        }

        return ((baseFare+extra)*criteria.getPassengerCountValue());
    }
}
