package com.tw.booking.service;

import com.tw.booking.entity.Flight;
import com.tw.booking.entity.FlightSchedule;
import com.tw.booking.models.FlightSearchCriteria;

public interface ICalculateFare {
    double calculateTotalFare(Flight flight, FlightSchedule schedule, FlightSearchCriteria criteria);
}
