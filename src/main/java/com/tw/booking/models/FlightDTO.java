package com.tw.booking.models;

import lombok.*;

import java.time.LocalDate;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class FlightDTO {
    private int id;
    private String name;
    private String origin;
    private String destination;
    //private LocalDate departureDate;
    private String departureDate;
    private TravelType travelType;
    private double totalFare;

}
