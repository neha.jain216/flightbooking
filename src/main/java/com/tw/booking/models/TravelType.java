package com.tw.booking.models;

public enum TravelType {
    FIRST_CLASS,
    BUSINESS_CLASS,
    ECONOMY_CLASS;
}
