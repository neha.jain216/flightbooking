# FlightBooking
-to build jar file
> ./mvnw clean package

-to buld image
> docker image build -t flightbooking .

-run container
> docker run -p 9090:8080 flightbooking

-restart existing container
> docker container start <containername>